interface Car: Vehicle {
    override fun operate() {
        println("Drive!")
    }
}

class Audi : Car {
    override fun operate() {
        println("Drive Audi!")
    }
}

class Maserati : Car {
    override fun operate() {
        println("Drive Maserati!")
    }
}

class CarFactory: AbstractVehicleFactory() {
    override fun create(model: String): Car? {
        return when (model) {
            "Audi" -> Audi()
            "Maserati" -> Maserati()
            else -> null
        }
    }
}
