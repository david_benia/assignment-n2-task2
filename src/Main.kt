fun main(){
    val new_land_vehicle = VehicleProvider.getVehicle("Car")
    val new_air_vehicle = VehicleProvider.getVehicle("Aircraft")

    val new_audi = new_land_vehicle.create("Audi")
    val new_boeing = new_air_vehicle.create("Boeing737")

    new_audi!!.operate()
    new_boeing!!.operate()
}
