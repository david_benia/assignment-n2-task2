import java.lang.Exception

interface Vehicle {
    fun operate()
}

abstract class AbstractVehicleFactory {
    abstract fun create(any: String): Vehicle?
}

class VehicleProvider {
    companion object {
        fun getVehicle(type: String): AbstractVehicleFactory {
            return when (type) {
                "Car" -> CarFactory()
                "Aircraft" -> AircraftFactory()
                else -> throw Exception("Unknown vehicle type")
            }
        }
    }
}