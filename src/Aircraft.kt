interface Aircraft: Vehicle {
    override fun operate() {
        println("Fly!")
    }
}

class Boeing737 : Aircraft {
    override fun operate() {
        println("Fly Boeing737!")
    }
}

class Boeing777 : Aircraft {
    override fun operate() {
        println("Fly Boeing777!")
    }
}

class AircraftFactory: AbstractVehicleFactory() {
     override fun create(model: String): Aircraft? {
        return when (model) {
            "Boeing737" -> Boeing737()
            "Boeing777" -> Boeing777()
            else -> null
        }
    }
}